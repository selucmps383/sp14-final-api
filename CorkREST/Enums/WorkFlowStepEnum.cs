﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.Enums
{
    public enum WorkFlowStepEnum
    {
       Found = 1,
       InProgress = 2,
       NotReproducible = 3,
       ReadyforTesting = 4,
	   FixFailed = 5,
	   ClosedAsDesigned = 6,
	   ClosedNotReproducible = 7,
	   ClosedFixed

    }
}