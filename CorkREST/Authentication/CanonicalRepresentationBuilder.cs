﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace CorkREST.Authentication
{
    /// <summary>
    /// Class container for the Build Representation call
    /// </summary>
    /// <returns></returns>
    public class CanonicalRepresentationBuilder : IBuildMessageRepresentation
    {
        /// <summary>
        /// Builds message representation as follows:
        /// HTTP VERB\n + URI\n + BODY
        /// </summary>
        /// <returns> The joined string</returns>
        public string  BuildRequestRepresentation(HttpRequestMessage requestMessage)
        {
            //string content =  requestMessage.Content.ToString();
      
            string httpMethod = requestMessage.Method.Method;

            string uri = requestMessage.RequestUri.AbsolutePath;

            string representation = httpMethod + "\n" + uri + "\n";// +content;

            return representation;
        }
    }
}